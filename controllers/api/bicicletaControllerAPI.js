var Bicicleta = require('../../models/bicicleta');

// Listar bicicletas
exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

// crear bicicletas
exports.bicicleta_create = function(req, res) {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}

// actualizar bicicletas
exports.bicicleta_update = function(req, res) {
    let { id, color, modelo, lat, lng } = req.body
    var bici = new Bicicleta(id, color, modelo, [lat, lng])

    Bicicleta.updateBici(bici)
    res.status(200).json({
        bicicleta: bici
    })
};

// Eliminar bicicletas
exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}